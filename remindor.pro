FORMS = data/ui/about_qt.ui\
        data/ui/command_qt.ui\
        data/ui/date_qt.ui\
        data/ui/preferences_qt.ui\
        data/ui/quick_qt.ui\
        data/ui/reminder_qt.ui\
        data/ui/manage_qt.ui\
        data/ui/time_qt.ui

RESOURCES += \
    resources.qrc
