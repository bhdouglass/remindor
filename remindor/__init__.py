# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2014 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

use_dbus = True
try:
    import dbus
except:
    use_dbus = False

import sys

import gettext
from gettext import gettext as _
gettext.textdomain('remindor-common')

from agui import APP
from .remindor_common import helpers, DBusService

__version__ = '14.02'

def main():
    args = helpers.parse_args('remindor', __version__, use_dbus)

    if args.qt:
        APP.gui = APP.PYSIDE
    elif args.gtk:
        APP.gui = APP.GTK
    else:
        print('No gui chosen, assuming gtk')
        APP.gui = APP.GTK

    APP.tr = _

    if use_dbus and (args.add or args.quick or args.manage or args.preferences or args.stop):
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        ds = DBusService()

        if args.add:
            #TODO: dialog
            ds.emitUpdate()
            sys.exit(0)

        elif args.simple:
            #TODO: dialog
            ds.emitUpdate()
            sys.exit(0)

        elif args.quick:
            #TODO: dialog
            ds.emitUpdate()
            sys.exit(0)

        elif args.manage:
            ds.emitManage()
            sys.exit(0)

        elif args.preferences:
            #TODO: dialog
            ds.emitUpdate()
            sys.exit(0)

        elif args.stop:
            ds.emitStop()
            sys.exit(0)

        elif args.update:
            ds.emitUpdate()
            sys.exit(0)

    else:
        helpers.migrate_config()
        helpers.set_up_logging('remindor', args, __version__)

        from remindor.manage_window import ManageWindow
        mw = ManageWindow()

    #sys.exit(0)
    APP.run()
