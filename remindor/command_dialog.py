# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2014 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import gettext
from gettext import gettext as _
gettext.textdomain('remindor-common')

from agui import Signal, APP
from agui.widgets import Window

from remindor import functions

class CommandDialog(object):
    insert_values = ['{date}', '{month}', '{monthname}', '{day}', '{dayname}', '{yearday}',
                     '{year}', '{time}', '{hour}', '{hour12}', '{minute}', '{second}',
                     '{microsecond}', '{soundfullfile}', '{soundfile}']

    def __init__(self, command, parent=None):
        self.done = Signal()

        self.dialog = Window('command_dialog', functions.ui_file('command'), parent) #TODO: translate window
        self.dialog.closed.connect(self.dialog.close)

        self.dialog.widgets.command_edit.has_clear = True
        self.dialog.widgets.command_edit.text = command
        self.dialog.widgets.insert_combo.selected = 0

        self.dialog.widgets.insert_button.activated.connect(self.insert)
        self.dialog.widgets.cancel_button.activated.connect(self.dialog.close)
        self.dialog.widgets.ok_button.activated.connect(self.ok)

        if APP.is_gtk():
            self.dialog.other_widgets.appchooser.connect('application-selected', self.select_app)

        self.dialog.show()

    def select_app(self, info=None, data=None):
        if APP.is_gtk():
            app = self.dialog.other_widgets.appchooser.get_app_info()
            command = app.get_executable()
            text = self.dialog.widgets.command_edit.text

            if text != '':
                self.dialog.widgets.command_edit.text = '%s %s' % (command, self.dialog.widgets.command_edit.text)
            else:
                self.dialog.widgets.command_edit.text = command

    def insert(self):
        index = self.dialog.widgets.insert_combo.selected
        self.dialog.widgets.command_edit.insert_at_cursor(self.insert_values[index])

    def ok(self):
        self.done.emit(self.dialog.widgets.command_edit.text)

        self.dialog.close()
