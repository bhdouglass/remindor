# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2014 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import gettext
from gettext import gettext as _
gettext.textdomain('remindor-common')

from agui import Signal, APP
from agui.widgets import Window

from remindor import functions
from .remindor_common.ui_helpers import DateDialogInfo

class DateDialog(object):
    def __init__(self, time_text, parent=None):
        self.info = DateDialogInfo(time_text)
        self.done = Signal()

        self.dialog = Window('date_dialog', functions.ui_file('date'), parent) #TODO: translate window
        self.dialog.closed.connect(self.dialog.close)

        self.dialog.widgets.date_combo.changed.connect(self.update)
        self.dialog.widgets.date_once_combo.changed.connect(self.update_once)

        if APP.is_gtk():
            self.dialog.widgets.date_once_edit.has_clear = True
            self.dialog.widgets.date_once_edit.has_error = True
            self.dialog.widgets.date_once_edit.hide_error()
            self.dialog.widgets.date_from_edit.has_clear = True
            self.dialog.widgets.date_from_edit.has_error = True
            self.dialog.widgets.date_from_edit.hide_error()
            self.dialog.widgets.date_to_edit.has_clear = True
            self.dialog.widgets.date_to_edit.has_error = True
            self.dialog.widgets.date_to_edit.hide_error()

            self.dialog.widgets.date_from_edit.changed.connect(self.validate)
            self.dialog.widgets.date_to_edit.changed.connect(self.validate)
            self.dialog.widgets.date_once_edit.changed.connect(self.validate)

            self.dialog.widgets.date_once_edit.text = self.info.once_date
            self.dialog.widgets.date_from_edit.text = self.info.from_date
            self.dialog.widgets.date_to_edit.text = self.info.to_date

        elif APP.is_pyside():
            self.dialog.widgets.date_once_datepicker.date = self.info.once_date_internal
            self.dialog.widgets.date_from_datepicker.date = self.info.from_date_internal
            self.dialog.widgets.date_to_datepicker.date = self.info.to_date_internal
            self.dialog.widgets.date_from_check.checked = (self.info.from_date_internal is not None and self.info.to_date_internal is not None)
            if self.info.active == self.info.next_days:
                self.dialog.widgets.date_from_check.checked = (self.info.from_date_internal is not None)

            self.dialog.widgets.date_once_datepicker.changed.connect(self.validate)
            self.dialog.widgets.date_from_datepicker.changed.connect(self.validate)
            self.dialog.widgets.date_to_datepicker.changed.connect(self.validate)

        self.dialog.widgets.cancel_button.activated.connect(self.dialog.close)
        self.dialog.widgets.ok_button.activated.connect(self.ok)

        self.dialog.widgets.date_combo.selected = self.info.active
        self.dialog.widgets.date_once_combo.selected = self.info.once_active
        self.dialog.widgets.date_every_spin.value = self.info.every_spin
        self.dialog.widgets.date_every_combo.selected = self.info.every_active

        self.update(self.dialog.widgets.date_combo.selected)
        self.dialog.show()

    def update(self, selected):
        self.dialog.widgets.date_on_label.hide()
        self.dialog.widgets.date_once_combo.hide()
        self.dialog.widgets.date_every_spin_label.hide()
        self.dialog.widgets.date_every_spin.hide()
        self.dialog.widgets.date_days_label.hide()
        self.dialog.widgets.date_every_combo_label.hide()
        self.dialog.widgets.date_every_combo.hide()
        self.dialog.widgets.date_from_label.hide()
        self.dialog.widgets.date_to_label.hide()
        if APP.is_gtk():
            self.dialog.widgets.date_from_edit.hide()
            self.dialog.widgets.date_to_edit.hide()
            self.dialog.widgets.date_once_edit.hide()
        elif APP.is_pyside():
            self.dialog.widgets.date_once_datepicker.hide()
            self.dialog.widgets.date_from_datepicker.hide()
            self.dialog.widgets.date_to_datepicker.hide()
            self.dialog.widgets.date_from_check.hide()

        if selected == self.info.once:
            self.dialog.widgets.date_on_label.show()
            self.dialog.widgets.date_once_combo.show()
            self.update_once(self.dialog.widgets.date_once_combo.selected)

        else:
            self.dialog.widgets.date_from_label.show()
            self.dialog.widgets.date_to_label.show()

            if APP.is_gtk():
                self.dialog.widgets.date_from_edit.show()
                self.dialog.widgets.date_to_edit.show()
            elif APP.is_pyside():
                self.dialog.widgets.date_from_datepicker.show()
                self.dialog.widgets.date_to_datepicker.show()
                self.dialog.widgets.date_from_check.show()

        if selected == self.info.every_days:
            self.dialog.widgets.date_every_spin_label.show()
            self.dialog.widgets.date_every_spin.show()
            self.dialog.widgets.date_days_label.text = _('Day(s)')

        elif selected == self.info.every_month:
            self.dialog.widgets.date_every_spin_label.show()
            self.dialog.widgets.date_every_spin_label.text = _('Every')
            self.dialog.widgets.date_every_spin.show()
            self.dialog.widgets.date_days_label.text = _('of the month')

        elif selected == self.info.every:
            self.dialog.widgets.date_every_combo_label.show()
            self.dialog.widgets.date_every_combo.show()

        elif selected == self.info.next_days:
            self.dialog.widgets.date_every_spin_label.show()
            self.dialog.widgets.date_every_spin_label.text = _('Next')
            self.dialog.widgets.date_every_spin.show()
            self.dialog.widgets.date_days_label.text = _('Day(s)')
            self.dialog.widgets.date_to_label.hide()

            if APP.is_gtk():
                self.dialog.widgets.date_to_edit.hide()
            elif APP.is_pyside():
                self.dialog.widgets.date_to_datepicker.hide()

        self.validate()
        self.dialog.resize(1, 1)

    def update_once(self, selected, validate=True):
        if selected == 0:
            if APP.is_gtk():
                self.dialog.widgets.date_once_edit.show()
            elif APP.is_pyside():
                self.dialog.widgets.date_once_datepicker.show()
        else:
            if APP.is_gtk():
                self.dialog.widgets.date_once_edit.hide()
            elif APP.is_pyside():
                self.dialog.widgets.date_once_datepicker.hide()

        if validate:
            self.validate()

    def validate(self, text=''):
        once_text = ''
        from_text = ''
        to_text = ''
        if APP.is_gtk():
            self.dialog.widgets.date_once_edit.hide_error()
            self.dialog.widgets.date_from_edit.hide_error()
            self.dialog.widgets.date_to_edit.hide_error()

            once_text = self.dialog.widgets.date_once_edit.text
            from_text = self.dialog.widgets.date_from_edit.text
            to_text = self.dialog.widgets.date_to_edit.text

        elif APP.is_pyside():
            once_text = self.dialog.widgets.date_once_datepicker.date
            from_text = ''
            to_text = ''

            if self.dialog.widgets.date_from_check.checked:
                from_text = self.dialog.widgets.date_from_datepicker.date
                to_text = self.dialog.widgets.date_to_datepicker.date

        self.dialog.widgets.error_label.hide()

        error = self.info.validate_date(self.dialog.widgets.date_combo.selected,
            self.dialog.widgets.date_once_combo.selected, once_text, from_text, to_text)

        if error == self.info.once_error and APP.is_gtk():
            self.dialog.widgets.date_once_edit.show_error()
        elif error == self.info.from_error and APP.is_gtk():
            self.dialog.widgets.date_from_edit.show_error()
        elif error == self.info.to_error and APP.is_gtk():
            self.dialog.widgets.date_to_edit.show_error()
        elif error == self.info.order_error:
            self.dialog.widgets.error_label.show()
        elif error == self.info.from_to_error and APP.is_gtk():
            self.dialog.widgets.date_once_edit.show_error()
            self.dialog.widgets.date_from_edit.show_error()

        if error == self.info.no_error:
            self.dialog.widgets.ok_button.enable()
        else:
            self.dialog.widgets.ok_button.disable()

    def ok(self):
        once_text = ''
        from_text = ''
        to_text = ''
        if APP.is_gtk():
            once_text = self.dialog.widgets.date_once_edit.text
            from_text = self.dialog.widgets.date_from_edit.text
            to_text = self.dialog.widgets.date_to_edit.text
        elif APP.is_pyside():
            once_text = self.dialog.widgets.date_once_datepicker.date
            from_text = ''
            to_text = ''

            if self.dialog.widgets.date_from_check.checked:
                from_text = self.dialog.widgets.date_from_datepicker.date
                to_text = self.dialog.widgets.date_to_datepicker.date

        self.done.emit(self.info.build_date(self.dialog.widgets.date_combo.selected,
            self.dialog.widgets.date_once_combo.selected, once_text,
            self.dialog.widgets.date_every_combo.selected, self.dialog.widgets.date_every_spin.value,
            from_text, to_text))

        self.dialog.close()
