# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2014 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import os, sys

from agui import APP
from agui.extras import Icon

def data_dir():
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../data/'))
    if not os.path.exists(path):
        path = os.path.join(sys.prefix, 'share/remindor')

    return path

def logo_file():
    filename = 'remindor_gtk.svg'
    if APP.is_pyside():
        filename = 'remindor_qt.svg'

    return img_file(filename)

def logo_icon():
    return Icon('remindor', logo_file())

def img_file(filename):
    return os.path.join(data_dir(), 'img', filename)

def ui_file(filename):
    if APP.is_gtk():
        filename = '%s_gtk.ui' % filename
    elif APP.is_pyside():
        filename = '%s_qt.ui' % filename

    return os.path.join(data_dir(), 'ui', filename)

def help(page):
    pass #TODO
