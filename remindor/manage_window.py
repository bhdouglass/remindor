# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2014 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import gettext
from gettext import gettext as _
gettext.textdomain('remindor-common')

from agui import APP
from agui.widgets import Window, Indicator
from agui.extras import Timeout, Icon, Message

from remindor import functions
from remindor.scheduler import Scheduler
from remindor.reminder_dialog import ReminderDialog
from remindor.quick_dialog import QuickDialog
from remindor.simple_dialog import SimpleDialog
from remindor.preferences_dialog import PreferencesDialog

from .remindor_common.ui_helpers import ManageWindowInfo
from .remindor_common import helpers, constants

class ManageWindow(object):
    def __init__(self):
        self.window = Window('manage_window', functions.ui_file('manage')) #TODO: translate window
        #self.window.closed.connect(self.window.hide)
        self.window.closed.connect(self.quit) #TODO: switch to hide

        self.window.widgets.add_action.activated.connect(self.add)
        self.window.widgets.simple_add_action.activated.connect(self.simple_add)
        self.window.widgets.quick_add_action.activated.connect(self.quick_add)
        self.window.widgets.close_action.activated.connect(self.window.hide)
        self.window.widgets.quit_action.activated.connect(self.quit)

        self.window.widgets.edit_action.activated.connect(self.edit)
        self.window.widgets.postpone_action.activated.connect(self.postpone)
        self.window.widgets.delete_action.activated.connect(self.delete)
        self.window.widgets.preferences_action.activated.connect(self.preferences)

        self.window.widgets.refresh_action.activated.connect(self.refresh)
        self.window.widgets.clear_icon_action.activated.connect(self.clear_icon)
        self.window.widgets.stop_sound_action.activated.connect(self.stop_sound)

        self.window.widgets.help_action.activated.connect(self.help)
        self.window.widgets.question_action.activated.connect(self.question)
        self.window.widgets.bug_action.activated.connect(self.bug)
        self.window.widgets.website_action.activated.connect(self.website)
        self.window.widgets.about_action.activated.connect(self.about)

        self.window.widgets.reminder_tree.double_button_press.connect(self.edit)
        self.window.widgets.reminder_tree.column_width(0, 200)
        self.window.widgets.reminder_tree.context_menu = self.window.widgets.context_menu

        attention_icon = Icon('indicator-remindor-attention', functions.img_file('remindor-attention.svg')) #TODO use real name
        passive_icon = Icon('indicator-remindor-active', functions.img_file('remindor-active.svg')) #TODO: use settings & real name
        self.indicator = None
        self.indicator = Indicator(self.window, 'remindor', self.window.widgets.indicator_menu, attention_icon, passive_icon)
        self.indicator.show()

        self.scheduler = Scheduler(self.indicator)
        self.info = ManageWindowInfo(self.scheduler, self.update)
        self.info.update(True)
        self.scheduler.updated.connect(self.info.update)

        self.past_due()

        self.refresh_timer = Timeout(60 * 60 * 6, self.update, True) #1/4 day refresh
        self.refresh_timer.start()

        self.window.resize(700, 300)
        self.window.show() #TODO: don't show

    def quit(self):
        APP.quit()

    def add(self):
        self.reminder_dialog = ReminderDialog(None, self.window)
        self.reminder_dialog.saved.connect(self.info.add)

    def simple_add(self):
        self.reminder_dialog = SimpleDialog(self.window)
        self.reminder_dialog.saved.connect(self.info.add)

    def quick_add(self):
        self.quick_dialog = QuickDialog(self.window)
        self.quick_dialog.saved.connect(self.info.add)

    def edit(self, button=None, x=None, y=None):
        print(self.window.widgets.reminder_tree.selected)
        if self.window.widgets.reminder_tree.selected is not None and self.window.widgets.reminder_tree.selected >= 0:
            self.reminder_dialog = ReminderDialog(self.window.widgets.reminder_tree.selected, self.window)
            self.reminder_dialog.saved.connect(self.info.edit)

    def postpone(self):
        if self.window.widgets.reminder_tree.selected is not None and self.window.widgets.reminder_tree.selected >= 0:
            message = self.info.postpone(self.window.widgets.reminder_tree.selected)

            if message:
                Message().message_alt('Remindor', _("Sorry, you cannot postpone a repeating time."), functions.logo_icon(), self.window)

    def delete(self):
        if self.window.widgets.reminder_tree.selected is not None and self.window.widgets.reminder_tree.selected >= 0:
            self.info.delete(self.window.widgets.reminder_tree.selected)

    def preferences(self):
        self.preferences_dialog = PreferencesDialog(self.window)
        self.preferences_dialog.saved.connect(self.info.update)

    def refresh(self):
        self.info.update(True)

    def clear_icon(self):
        self.scheduler.clear_icon()

    def stop_sound(self):
        self.scheduler.stop_sound()

    def help(self):
        functions.help('main')

    def question(self):
        helpers.open_url(constants.question_site)

    def bug(self):
        helpers.open_url(constants.bug_site)

    def website(self):
        helpers.open_url(constants.website)

    def about(self):
        print('about') #TODO

    def update(self, add_timeout=False):
        self.window.widgets.reminder_tree.clear()
        today = self.window.widgets.reminder_tree.add_row(-1, [_("Today's Reminders"), '', '', ''], '', self.info.today_color_hex)
        future = self.window.widgets.reminder_tree.add_row(-2, [_('Future Reminders'), '', '', ''], '', self.info.future_color_hex)
        past = self.window.widgets.reminder_tree.add_row(-3, [_('Past Reminders'), '', '', ''], '', self.info.past_color_hex)

        #Make the color match the qt window
        color = ''
        if APP.is_gtk():
            color = '#FFFFFF'

        for id, reminder in self.info.reminder_list.items():
            parent = None
            if reminder.parent == self.info.today:
                parent = today
            elif reminder.parent == self.info.future:
                parent = future
            elif reminder.parent == self.info.past:
                parent = past

            self.window.widgets.reminder_tree.add_row(reminder.id, reminder.display_data(), reminder.tooltip(self.info.settings), color, parent)

        self.window.widgets.reminder_tree.expand_all()

        if add_timeout:
            self.refresh_timer = Timeout(60 * 60 * 6, self.update, True)

    def past_due(self):
        message = self.info.past_due()
        if message:
            Message().message('Remindor', _('While you were gone:'), message, functions.logo_icon(), self.window)
