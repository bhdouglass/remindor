# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2014 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import gettext
from gettext import gettext as _
gettext.textdomain('remindor-common')

from agui import Signal
from agui.widgets import Window, FileChooser, ColorChooser
from agui.extras import Icon, Message

from remindor import functions
from remindor.time_dialog import TimeDialog

from .remindor_common.ui_helpers import PreferencesDialogInfo
from .remindor_common import helpers
from .remindor_common.datetimeutil import Date, Time

class PreferencesDialog(object):
    def __init__(self, parent=None):
        self.info = PreferencesDialogInfo()
        self.saved = Signal()

        self.dialog = Window('preferences_dialog', functions.ui_file('preferences'), parent) #TODO: translate window
        self.dialog.closed.connect(self.dialog.close)
        self.dialog.promote('sound_file_chooser', FileChooser)
        self.dialog.promote('today_color_chooser', ColorChooser)
        self.dialog.promote('future_color_chooser', ColorChooser)
        self.dialog.promote('past_color_chooser', ColorChooser)

        self.dialog.widgets.label_edit.has_clear = True
        self.dialog.widgets.time_edit.has_clear = True
        self.dialog.widgets.time_edit.has_error = True
        self.dialog.widgets.time_edit.hide_error()
        self.dialog.widgets.date_edit.has_clear = True
        self.dialog.widgets.date_edit.has_clear = True
        self.dialog.widgets.date_edit.hide_error()
        self.dialog.widgets.command_edit.has_clear = True
        self.dialog.widgets.quick_label_edit.has_clear = True
        self.dialog.widgets.boxcar_email_edit.has_clear = True
        self.dialog.widgets.pushbullet_api_key_edit.has_clear = True

        self.dialog.widgets.time_edit.changed.connect(self.validate)
        self.dialog.widgets.date_edit.changed.connect(self.validate)
        self.dialog.widgets.time_edit_action.activated.connect(self.time_edit)
        self.dialog.widgets.date_edit_action.activated.connect(self.date_edit)
        self.dialog.widgets.command_edit_action.activated.connect(self.command_edit)
        self.dialog.widgets.sound_clear_action.activated.connect(self.dialog.widgets.sound_file_chooser.clear)
        self.dialog.widgets.pushbullet_refresh_action.activated.connect(self.pushbullet_refresh)
        self.dialog.widgets.help_action.activated.connect(self.help)
        self.dialog.widgets.cancel_action.activated.connect(self.dialog.close)
        self.dialog.widgets.save_action.activated.connect(self.save)

        self.dialog.widgets.label_edit.text = self.info.settings.label
        self.dialog.widgets.time_edit.text = self.info.settings.time
        self.dialog.widgets.date_edit.text = self.info.settings.date
        self.dialog.widgets.command_edit.text = self.info.settings.command
        self.dialog.widgets.postpone_spin.value = self.info.settings.postpone

        self.dialog.widgets.popup_check.checked = self.info.settings.popup
        self.dialog.widgets.dialog_check.checked = self.info.settings.dialog
        self.dialog.widgets.change_icon_check.checked = self.info.settings.change_indicator

        self.dialog.widgets.quick_label_edit.text = self.info.settings.quick_label
        self.dialog.widgets.quick_time_spin.value = self.info.settings.quick_minutes
        self.dialog.widgets.quick_unit_combo.selected = self.info.settings.quick_unit
        self.dialog.widgets.quick_slider_check.checked = self.info.settings.quick_slider
        self.dialog.widgets.quick_popup_check.checked = self.info.settings.quick_popup
        self.dialog.widgets.quick_dialog_check.checked = self.info.settings.quick_dialog
        self.dialog.widgets.quick_sound_check.checked = self.info.settings.quick_sound
        self.dialog.widgets.quick_info_check.checked = self.info.settings.quick_info

        self.dialog.widgets.simple_popup_check.checked = self.info.settings.simple_popup
        self.dialog.widgets.simple_dialog_check.checked = self.info.settings.simple_dialog
        self.dialog.widgets.simple_sound_check.checked = self.info.settings.simple_sound

        if self.info.settings.sound_file is not None and not self.info.settings.sound_file == "":
            self.dialog.widgets.sound_file_chooser.file = self.info.settings.sound_file
        else:
            self.dialog.widgets.sound_file_chooser.clear()
            self.dialog.widgets.sound_file_chooser.dir = helpers.sounds_dir()

        self.dialog.widgets.length_spin.value = self.info.settings.sound_play_length
        self.dialog.widgets.loop_check.checked = self.info.settings.sound_loop
        self.dialog.widgets.loop_times_spin.value = self.info.settings.sound_loop_times

        self.dialog.widgets.today_color_chooser.color_hex = self.info.settings.today_color
        self.dialog.widgets.future_color_chooser.color_hex = self.info.settings.future_color
        self.dialog.widgets.past_color_chooser.color_hex = self.info.settings.past_color
        #self.dialog.widgets.news_check.checked = self.info.settings.show_news
        self.dialog.widgets.indicator_icon_combo.selected = self.info.settings.indicator_icon
        self.dialog.widgets.hide_indicator_check.checked = self.info.settings.hide_indicator
        self.hide_start = self.info.settings.hide_indicator

        self.dialog.widgets.time_format_combo.selected = self.info.settings.time_format
        self.dialog.widgets.date_format_combo.selected = self.info.settings.date_format

        self.dialog.widgets.boxcar_email_edit.text = self.info.settings.boxcar_email
        self.dialog.widgets.boxcar_notification_check.checked = self.info.settings.boxcar_notify
        self.dialog.widgets.pushbullet_api_key_edit.text = self.info.settings.pushbullet_api_key
        self.dialog.widgets.pushbullet_device_combo.selected = self.info.pushbullet_device_index

        self.pushbullet_refresh_combobox(self.info.settings.pushbullet_devices)

        self.dialog.show()

    def validate(self, widget=None):
        enabled = True
        time = Time(self.dialog.widgets.time_edit.text, self.info.settings.time_format_object())
        date = Date(self.dialog.widgets.date_edit.text, self.info.settings.date_format_object())

        if time.is_valid():
            self.dialog.widgets.time_edit.hide_error()
        else:
            self.dialog.widgets.time_edit.show_error()
            enabled = False

        if date.is_valid():
            self.dialog.widgets.date_edit.hide_error()
        else:
            self.dialog.widgets.date_edit.show_error()
            enabled = False

        self.dialog.widgets.save_button.enabled = enabled

    def time_edit(self):
        self.time_dialog = TimeDialog(self.dialog.widgets.time_edit.text, self.dialog)
        self.time_dialog.done.connect(self.time_update)

    def time_update(self, text):
        self.dialog.widgets.time_edit.text = text

    def date_edit(self):
        pass #TODO

    def command_edit(self):
        pass #TODO

    def pushbullet_refresh(self):
        if self.dialog.widgets.pushbullet_api_key_edit.text == '':
            message = _('Please provide a Pushbullet api key.')
            Message().message_alt('Pushbullet', message, Icon('dialog-info'), self.dialog)
        else:
            self.validate_pushbullet()
            self.pushbullet_refresh_combobox(self.info.settings.pushbullet_devices)

    def pushbullet_refresh_combobox(self, devices):
        devices = list(devices)
        devices.insert(0, {'id': -1, 'name': _('None')})

        self.dialog.widgets.pushbullet_device_combo.clear()
        for device in devices:
            self.dialog.widgets.pushbullet_device_combo.append(device['name'])

        self.dialog.widgets.pushbullet_device_combo.selected = self.info.pushbullet_device_index

    def help(self):
        functions.help('preferences')

    def save(self):
        hide = self.dialog.widgets.hide_indicator_check.checked
        ok = True
        if hide and not self.hide_start:
            message = _('You have chosen to hide the indicator.\nOnly use this option if you know what you are doing!\n\nYou can run the command \"%s -p\"\nfrom the command line to open the preferences dialog\nto change this option.\n\nWould you like to continue?') % 'remindor'
            ans = Message().yes_no(_('Hide Indicator'), message, self.dialog)
            if ans == Message.no:
                ok = False
                self.dialog.widgets.hide_indicator_check.checked = False

        if ok:
            self.info.settings.label = self.dialog.widgets.label_edit.text
            self.info.settings.time = self.dialog.widgets.time_edit.text
            self.info.settings.date = self.dialog.widgets.date_edit.text
            self.info.settings.command = self.dialog.widgets.command_edit.text
            self.info.settings.postpone = self.dialog.widgets.postpone_spin.value

            self.info.settings.popup = self.dialog.widgets.popup_check.checked
            self.info.settings.dialog = self.dialog.widgets.dialog_check.checked
            self.info.settings.change_indicator = self.dialog.widgets.change_icon_check.checked

            self.info.settings.quick_label = self.dialog.widgets.quick_label_edit.text
            self.info.settings.quick_minutes = self.dialog.widgets.quick_time_spin.value
            self.info.settings.quick_unit = self.dialog.widgets.quick_unit_combo.selected
            self.info.settings.quick_slider = self.dialog.widgets.quick_slider_check.checked
            self.info.settings.quick_popup = self.dialog.widgets.quick_popup_check.checked
            self.info.settings.quick_dialog = self.dialog.widgets.quick_dialog_check.checked
            self.info.settings.quick_sound = self.dialog.widgets.quick_sound_check.checked
            self.info.settings.quick_info = self.dialog.widgets.quick_info_check.checked

            self.info.settings.simple_popup = self.dialog.widgets.simple_popup_check.checked
            self.info.settings.simple_dialog = self.dialog.widgets.simple_dialog_check.checked
            self.info.settings.simple_sound = self.dialog.widgets.simple_sound_check.checked

            self.info.settings.sound_file = self.dialog.widgets.sound_file_chooser.file
            self.info.settings.sound_play_length = self.dialog.widgets.length_spin.value
            self.info.settings.sound_loop = self.dialog.widgets.loop_check.checked
            self.info.settings.sound_loop_times = self.dialog.widgets.loop_times_spin.value

            self.info.settings.today_color = self.dialog.widgets.today_color_chooser.color_hex
            self.info.settings.future_color = self.dialog.widgets.future_color_chooser.color_hex
            self.info.settings.past_color = self.dialog.widgets.past_color_chooser.color_hex
            #self.info.settings.show_news = self.dialog.widgets.news_check.checked
            self.info.settings.indicator_icon = self.dialog.widgets.indicator_icon_combo.selected
            self.info.settings.hide_indicator = self.dialog.widgets.hide_indicator_check.checked

            self.info.settings.time_format = self.dialog.widgets.time_format_combo.selected
            self.info.settings.date_format = self.dialog.widgets.date_format_combo.selected

            (boxcar_valid, boxcar_email, boxcar_notify) = self.validate_boxcar()
            self.info.settings.boxcar_email = boxcar_email
            self.info.settings.boxcar_notify = boxcar_notify

            (pushbullet_valid, pushbullet_api_key, pushbullet_device_index) = self.validate_pushbullet()
            self.info.settings.pushbullet_api_key = pushbullet_api_key
            self.info.settings.pushbullet_device = self.info.get_pushbullet_id(pushbullet_device_index, self.info.settings.pushbullet_devices)

            if boxcar_valid and pushbullet_valid:
                status = self.info.preferences()
                if status == self.info.time_error:
                    self.dialog.widgets.time_edit.focus()
                elif status == self.info.date_error:
                    self.dialog.widgets.date_edit.focus()
                else:
                    self.saved.emit()
                    self.dialog.close()

    def validate_boxcar(self):
        ok = True
        boxcar_email = self.dialog.widgets.boxcar_email_edit.text
        boxcar_notify = self.dialog.widgets.boxcar_notification_check.checked

        (status, boxcar_email, boxcar_notify) = self.info.validate_boxcar(boxcar_email, boxcar_notify)
        if status == self.info.boxcar_subscribe:
            message = _("It seems that you are not yet signed up for Boxcar.\nPlease visit boxcar.io to signup.")
            Message().message_alt('Boxcar', message, Icon('dialog-info'), self.dialog)
            ok = False
        elif status == self.info.boxcar_error:
            message = _("There has been a error connecting with Boxcar,\nplease check your email address or network connection.")
            Message().message_alt('Boxcar', message, Icon('dialog-warning'), self.dialog)
            ok = False

        return (ok, boxcar_email, boxcar_notify)

    def validate_pushbullet(self):
        ok = True
        pushbullet_api_key = self.dialog.widgets.pushbullet_api_key_edit.text
        pushbullet_device_index = self.dialog.widgets.pushbullet_device_combo.selected

        message = ''
        status = self.info.validate_pushbullet(pushbullet_api_key)
        if status != self.info.pushbullet_add:
            ok = False
            pushbullet_device_index = 0
            pushbullet_api_key = ''

        if status == self.info.pushbullet_delete:
            ok = True

        if status == self.info.pushbullet_invalid:
            message = _('The api key you provided is invalid.\nPlease check your account on pushbullet.com.')
        elif status == self.info.pushbullet_error:
            message = _('There was an error connecting with Pushbullet.\nPlease check your api key or network connection.')
        elif status == self.info.pushbullet_retry:
            message = _('There was an error connecting with Pushbullet.\nPlease try again later.')

        if message != '':
            Message().message_alt('Pushbullet', message, Icon('dialog-warning'), self.dialog)

        return (ok, pushbullet_api_key, pushbullet_device_index)
