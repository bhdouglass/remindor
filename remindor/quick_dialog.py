# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2014 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import gettext
from gettext import gettext as _
gettext.textdomain('remindor-common')

from agui import Signal
from agui.widgets import Window
from agui.extras import Icon

from remindor import functions
from .remindor_common.ui_helpers import QuickDialogInfo

class QuickDialog(object):
    def __init__(self, parent=None):
        self.info = QuickDialogInfo()
        self.saved = Signal()

        self.dialog = Window('quick_dialog', functions.ui_file('quick'), parent) #TODO: translate window
        self.dialog.closed.connect(self.dialog.close)

        self.dialog.widgets.label_edit.has_clear = True
        self.dialog.widgets.cancel_button.activated.connect(self.dialog.close)
        self.dialog.widgets.add_button.activated.connect(self.save)

        if self.info.show_slider:
            self.dialog.widgets.minutes_spin.hide()
            self.dialog.widgets.minutes_slider.show()
        else:
            self.dialog.widgets.minutes_spin.show()
            self.dialog.widgets.minutes_slider.hide()

        self.dialog.widgets.message_label.text = self.info.info
        if self.info.show_info:
            self.dialog.widgets.message_label.show()
        else:
            self.dialog.widgets.message_label.hide()

        self.dialog.widgets.label_edit.text = self.info.label
        self.dialog.widgets.minutes_spin.value = self.info.minutes
        self.dialog.widgets.minutes_slider.value = self.info.minutes
        self.dialog.widgets.time_combo.selected = self.info.unit

        self.dialog.show()

    def save(self):
        self.info.label = self.dialog.widgets.label_edit.text
        self.info.unit = self.dialog.widgets.time_combo.selected
        if self.info.show_slider:
            self.info.minutes = self.dialog.widgets.minutes_slider.value
        else:
            self.info.minutes = self.dialog.widgets.minutes_spin.value

        self.saved.emit(self.info.reminder())
        self.dialog.close()
