# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2014 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import gettext
from gettext import gettext as _
gettext.textdomain('remindor-common')

from agui import APP, Signal
from agui.widgets import Window, FileChooser
from agui.extras import Icon, Message

from remindor import functions
from remindor.time_dialog import TimeDialog
from remindor.date_dialog import DateDialog
from remindor.command_dialog import CommandDialog

from .remindor_common.ui_helpers import ReminderDialogInfo
from .remindor_common.datetimeutil import Date, Time
from .remindor_common import helpers

class ReminderDialog(object):
    def __init__(self, id=None, parent=None):
        self.id = id
        self.info = ReminderDialogInfo(self.id)
        self.saved = Signal()

        self.dialog = Window('reminder_dialog', functions.ui_file('reminder'), parent) #TODO: translate window
        self.dialog.closed.connect(self.dialog.close)
        self.dialog.promote('sound_file_chooser', FileChooser)

        if self.id is not None:
            self.dialog.title = _('Edit Reminder')
            self.dialog.widgets.save_button.show()
            self.dialog.widgets.add_button.hide()
        else:
            self.dialog.widgets.save_button.hide()
            self.dialog.widgets.add_button.show()

        self.dialog.widgets.label_edit.has_clear = True
        self.dialog.widgets.time_edit.has_clear = True
        self.dialog.widgets.time_edit.has_error = True
        self.dialog.widgets.time_edit.hide_error()
        self.dialog.widgets.date_edit.has_clear = True
        self.dialog.widgets.date_edit.has_error = True
        self.dialog.widgets.date_edit.hide_error()
        self.dialog.widgets.command_edit.has_clear = True

        self.dialog.widgets.time_edit.changed.connect(self.validate)
        self.dialog.widgets.date_edit.changed.connect(self.validate)
        self.dialog.widgets.time_button.pressed.connect(self.time_edit)
        self.dialog.widgets.date_button.pressed.connect(self.date_edit)
        self.dialog.widgets.command_button.pressed.connect(self.command_edit)
        self.dialog.widgets.insert_button.pressed.connect(self.insert)
        self.dialog.widgets.pushbullet_refresh_button.pressed.connect(self.pushbullet_refresh)
        self.dialog.widgets.play_sound_check.changed.connect(self.play_sound_changed)
        if 'clear_sound_button' in self.dialog.widgets:
            self.dialog.widgets.clear_sound_button.pressed.connect(self.dialog.widgets.sound_file_chooser.clear)
        self.dialog.widgets.loop_check.changed.connect(self.loop_check_changed)
        self.dialog.widgets.help_button.activated.connect(self.help)
        self.dialog.widgets.cancel_button.activated.connect(self.dialog.close)
        self.dialog.widgets.add_button.activated.connect(self.save)

        self.dialog.widgets.loop_info_label.text = '(will repeat %d times)' % (self.info.sound_loop_times)

        self.dialog.widgets.label_edit.text = self.info.label
        self.dialog.widgets.time_edit.text = self.info.user_time
        self.dialog.widgets.date_edit.text = self.info.user_date
        self.dialog.widgets.command_edit.text = self.info.command
        self.dialog.widgets.notes_edit.text = self.info.notes

        self.dialog.widgets.popup_check.checked = self.info.popup
        self.dialog.widgets.dialog_check.checked = self.info.dialog
        self.dialog.widgets.boxcar_check.checked = self.info.boxcar

        self.dialog.widgets.play_sound_check.checked = self.info.play_sound
        self.dialog.widgets.loop_check.checked = self.info.sound_loop
        self.dialog.widgets.play_length_spin.value = self.info.sound_length

        if self.info.sound_file is not None and not self.info.sound_file == "":
            self.dialog.widgets.sound_file_chooser.file = self.info.sound_file
        else:
            self.dialog.widgets.sound_file_chooser.clear()
            self.dialog.widgets.sound_file_chooser.dir = helpers.sounds_dir()

        if not self.info.boxcar_ok:
            self.dialog.widgets.boxcar_label.hide()
            self.dialog.widgets.boxcar_check.hide()

        if not self.info.pushbullet_ok:
            self.dialog.widgets.pushbullet_label.hide()
            self.dialog.widgets.pushbullet_combo.hide()
            self.dialog.widgets.pushbullet_refresh_button.hide()

        if not self.info.other_notify_label:
            self.dialog.widgets.other_notify_label.hide()

        self.play_sound_changed()
        self.loop_check_changed()

        self.dialog.show()

    def time_edit(self):
        self.time_dialog = TimeDialog(self.dialog.widgets.time_edit.text, self.dialog)
        self.time_dialog.done.connect(self.time_update)

    def time_update(self, text):
        self.dialog.widgets.time_edit.text = text

    def date_edit(self):
        self.date_dialog = DateDialog(self.dialog.widgets.date_edit.text, self.dialog)
        self.date_dialog.done.connect(self.date_update)

    def date_update(self, text):
        self.dialog.widgets.date_edit.text = text

    def command_edit(self):
        self.command_dialog = CommandDialog(self.dialog.widgets.command_edit.text, self.dialog)
        self.command_dialog.done.connect(self.command_update)

    def command_update(self, text):
        self.dialog.widgets.command_edit.text = text

    def insert(self):
        index = self.dialog.widgets.insert_combo.selected
        self.dialog.widgets.notes_edit.insert(helpers.insert_values[index])

    def pushbullet_refresh(self):
        self.info.refresh_pushbullet_devices(self.info.pushbullet_api_key)
        self.refresh_pushbullet_combobox()

    def clear_sound(self):
        self.dialog.widgets.sound_file_chooser.clear()

    def help(self):
        functions.help('reminder')

    def save(self):
        self.info.label = self.dialog.widgets.label_edit.text
        self.info.user_time = self.dialog.widgets.time_edit.text
        self.info.user_date = self.dialog.widgets.date_edit.text
        self.info.command = self.dialog.widgets.command_edit.text
        self.info.notes = self.dialog.widgets.notes_edit.text
        self.info.popup = self.dialog.widgets.popup_check.checked
        self.info.dialog = self.dialog.widgets.dialog_check.checked
        self.info.boxcar = self.dialog.widgets.boxcar_check.checked
        self.info.pushbullet_device = self.info.get_pushbullet_id(self.dialog.widgets.pushbullet_combo.selected, self.info.pushbullet_devices)
        self.info.play_sound = self.dialog.widgets.play_sound_check.checked
        self.info.sound_loop = self.dialog.widgets.loop_check.checked
        self.info.sound_length = self.dialog.widgets.play_length_spin.value
        self.info.sound_file = self.dialog.widgets.sound_file_chooser.file

        (status, id) = self.info.reminder(True)
        if status == self.info.ok:
            self.saved.emit(id)
            self.dialog.close()
        else:
            if status == self.info.file_error:
                title = _('File does not exist')
                message = ''
                if self.info.sound_file != '' and self.info.sound_file != None:
                    message = '%s\n\n%s' % (_('The following file does not exist.\nPlease choose another sound file.'), self.info.sound_file)
                else:
                    message = _('Please choose a sound file.')

                Message().message_alt(title, message, functions.logo_icon(), self.dialog)
            elif status == self.info.time_error or status == self.info.date_error:
                self.validate()
            elif status == self.info.notify_warn:
                message = _('The label and notes for this reminder are empty,\nwould you still like to use a notification?')
                ans = Message().yes_no(_('Empty Notification'), message)
                if ans == Message.yes:
                    (status, id) = self.info.reminder()
                    self.saved.emit(id)
                    self.dialog.close()

    def validate(self, text=None):
        enabled = True
        time = Time(self.dialog.widgets.time_edit.text, self.info.time_format)
        date = Date(self.dialog.widgets.date_edit.text, self.info.date_format)

        if time.is_valid():
            self.dialog.widgets.time_edit.hide_error()
        else:
            self.dialog.widgets.time_edit.show_error()
            enabled = False

        if date.is_valid():
            self.dialog.widgets.date_edit.hide_error()
        else:
            self.dialog.widgets.date_edit.show_error()
            enabled = False

        self.dialog.widgets.add_button.enabled = enabled
        self.dialog.widgets.save_button.enabled = enabled

    def play_sound_changed(self, checked=None):
        if self.dialog.widgets.play_sound_check.checked:
            self.dialog.widgets.loop_check.enable()
            self.dialog.widgets.play_length_spin.enable()
            self.dialog.widgets.sound_file_chooser.enable()

            if 'clear_sound_button' in self.dialog.widgets:
                self.dialog.widgets.clear_sound_button.enable()

            if 'sound_file_chooser_line_edit' in self.dialog.widgets:
                self.dialog.widgets.sound_file_chooser_line_edit.enable()
        else:
            self.dialog.widgets.loop_check.disable()
            self.dialog.widgets.play_length_spin.disable()
            self.dialog.widgets.sound_file_chooser.disable()

            if 'clear_sound_button' in self.dialog.widgets:
                self.dialog.widgets.clear_sound_button.disable()

            if 'sound_file_chooser_line_edit' in self.dialog.widgets:
                self.dialog.widgets.sound_file_chooser_line_edit.disable()

        self.loop_check_changed()

    def loop_check_changed(self, checked=None):
        if self.dialog.widgets.loop_check.checked:
            self.dialog.widgets.loop_info_label.show()
            self.dialog.widgets.play_length_spin.disable()
        else:
            self.dialog.widgets.loop_info_label.hide()
            if self.dialog.widgets.play_sound_check.checked:
                self.dialog.widgets.play_length_spin.enable()

    def refresh_pushbullet_combobox(self):
        devices = list(self.info.pushbullet_devices)
        devices.insert(0, {'id': -1, 'name': _('None')})

        self.dialog.widgets.pushbullet_combo.clear()
        for device in devices:
            self.dialog.widgets.pushbullet_combo.append(device['name'])

        self.dialog.widgets.pushbullet_combo.selected = self.info.pushbullet_device_index
