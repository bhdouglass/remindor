# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2014 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

from remindor import functions
from .remindor_common.scheduler import GenericScheduler

from agui.extras import Sound, Message, Popup, Icon, Timeout
from agui import Signal

import logging
logger = logging.getLogger('remindor')

class Scheduler(GenericScheduler):
    def __init__(self, indicator):
        GenericScheduler.__init__(self)
        self.indicator = indicator
        self.updated = Signal()
        self.player = None

    def remove_reminder_helper(self, reminder):
        if reminder.started():
            reminder.stop()

    def change_icon(self):
        self.indicator.attention = True

        if self.dbus_service != None:
            self.dbus_service.emitAttention()

    def clear_icon(self):
        self.indicator.attention = False

    def play_sound_helper(self, sound_file, sound_loop, sound_loop_times):
        self.player = Sound(sound_file, sound_loop_times)
        self.player.play()

    def stop_sound_helper(self):
        if self.player is not None:
            self.player.stop()

    def remove_playing_sound(self):
        self.playing_sound.stop()

    def add_playing_sound(self, length):
        self.playing_sound = Timeout(length, self.stop_sound)
        self.playing_sound.start()

    def popup_notification(self, label, notes):
        Popup().popup('remindor', label, notes, functions.logo_icon())

    def popup_dialog(self, label, notes):
        Message().message('Remindor', label, notes, functions.logo_icon())

    def update_schedule(self):
        self.updated.emit()

    def add_to_schedule(self, delay, id):
        if delay <= 172800:
            timeout = Timeout(delay, self.run_alarm, id)
            self.schedule[str(id)] = timeout
            timeout.start()
        else:
            logger.debug('not adding reminder with id=%s becacuse it is more than 2 days out' % str(id))
