# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2014 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

from agui import Signal
from agui.widgets import Window
from agui.extras import Icon

from remindor import functions
from .remindor_common.ui_helpers import SimpleDialogInfo

class SimpleDialog(object):
    def __init__(self, parent=None):
        self.info = SimpleDialogInfo()
        self.saved = Signal()

        self.dialog = Window('simple_dialog', functions.ui_file('simple'), parent) #TODO: translate window
        self.dialog.closed.connect(self.dialog.close)

        self.dialog.widgets.reminder_edit.has_clear = True
        self.dialog.widgets.reminder_edit.has_error = True
        self.dialog.widgets.reminder_edit.hide_error()
        self.dialog.widgets.reminder_edit.changed.connect(self.validate)

        self.dialog.widgets.help_button.activated.connect(self.help)
        self.dialog.widgets.cancel_button.activated.connect(self.dialog.close)
        self.dialog.widgets.add_button.activated.connect(self.save)

        self.dialog.resize(350, 50)
        self.dialog.show()

    def help(self):
        pass #TODO

    def validate(self, text):
        if self.info.validate(text):
            self.dialog.widgets.reminder_edit.hide_error()
            self.dialog.widgets.add_button.enable()
        else:
            self.dialog.widgets.reminder_edit.show_error()
            self.dialog.widgets.add_button.disable()

    def save(self):
        text = self.dialog.widgets.reminder_edit.text
        id = self.info.reminder(text)

        if id is not None:
            self.saved.emit(id)
            self.dialog.close()
        else:
            self.validate(self.dialog.widgets.reminder_edit.text)
