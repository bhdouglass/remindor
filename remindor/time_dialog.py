# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2014 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import gettext
from gettext import gettext as _
gettext.textdomain('remindor-common')

from agui import Signal, APP
from agui.widgets import Window

from remindor import functions
from .remindor_common.ui_helpers import TimeDialogInfo

class TimeDialog(object):
    def __init__(self, time_text, parent=None):
        self.info = TimeDialogInfo(time_text)
        self.done = Signal()

        self.dialog = Window('time_dialog', functions.ui_file('time'), parent) #TODO: translate window
        self.dialog.closed.connect(self.dialog.close)

        if APP.is_gtk():
            self.dialog.widgets.time_once_edit.has_clear = True
            self.dialog.widgets.time_once_edit.has_error = True
            self.dialog.widgets.time_once_edit.hide_error()
            self.dialog.widgets.time_from_edit.has_clear = True
            self.dialog.widgets.time_from_edit.has_error = True
            self.dialog.widgets.time_from_edit.hide_error()
            self.dialog.widgets.time_to_edit.has_clear = True
            self.dialog.widgets.time_to_edit.has_error = True
            self.dialog.widgets.time_to_edit.hide_error()

            self.dialog.widgets.time_once_edit.changed.connect(self.validate)
            self.dialog.widgets.time_from_edit.changed.connect(self.validate)
            self.dialog.widgets.time_to_edit.changed.connect(self.validate)

            self.dialog.widgets.time_once_edit.text = self.info.once_text
            self.dialog.widgets.time_from_edit.text = self.info.from_text
            self.dialog.widgets.time_to_edit.text = self.info.to_text

        elif APP.is_pyside():
            self.dialog.widgets.time_once_timepicker.time = self.info.once_text_internal
            self.dialog.widgets.time_from_timepicker.time = self.info.from_text_internal
            self.dialog.widgets.time_to_timepicker.time = self.info.to_text_internal
            self.dialog.widgets.time_from_check.checked = (self.info.from_text_internal is not None and self.info.to_text_internal is not None)

            self.dialog.widgets.time_once_timepicker.changed.connect(self.validate)
            self.dialog.widgets.time_from_timepicker.changed.connect(self.validate)
            self.dialog.widgets.time_to_timepicker.changed.connect(self.validate)

        self.dialog.widgets.error_label.hide()

        self.dialog.widgets.time_combo.changed.connect(self.update)
        self.dialog.widgets.cancel_button.activated.connect(self.dialog.close)
        self.dialog.widgets.ok_button.activated.connect(self.ok)

        self.dialog.widgets.time_every_spin.value = self.info.every
        self.dialog.widgets.time_combo.selected = self.info.active

        self.update(self.dialog.widgets.time_combo.selected)
        self.dialog.show()

    def update(self, selected):
        if selected == self.info.once:
            self.dialog.widgets.time_every_label.hide()
            self.dialog.widgets.time_every_spin.hide()
            self.dialog.widgets.time_unit_label.hide()
            self.dialog.widgets.time_from_label.hide()
            self.dialog.widgets.time_to_label.hide()

            self.dialog.widgets.time_at_label.show()
            if APP.is_gtk():
                self.dialog.widgets.time_once_edit.show()
                self.dialog.widgets.time_from_edit.hide()
                self.dialog.widgets.time_to_edit.hide()
            elif APP.is_pyside():
                self.dialog.widgets.time_once_timepicker.show()
                self.dialog.widgets.time_from_timepicker.hide()
                self.dialog.widgets.time_to_timepicker.hide()
                self.dialog.widgets.time_from_check.hide()

        elif selected == self.info.minutes or selected == self.info.hours:
            self.dialog.widgets.time_at_label.hide()
            if APP.is_gtk():
                self.dialog.widgets.time_once_edit.hide()
                self.dialog.widgets.time_from_edit.show()
                self.dialog.widgets.time_to_edit.show()
            elif APP.is_pyside():
                self.dialog.widgets.time_once_timepicker.hide()
                self.dialog.widgets.time_from_timepicker.show()
                self.dialog.widgets.time_to_timepicker.show()
                self.dialog.widgets.time_from_check.show()

            self.dialog.widgets.time_every_label.show()
            self.dialog.widgets.time_every_spin.show()
            self.dialog.widgets.time_unit_label.show()
            self.dialog.widgets.time_from_label.show()
            self.dialog.widgets.time_to_label.show()

            if selected == self.info.minutes:
                self.dialog.widgets.time_unit_label.text = _('Minute(s)')
            elif selected == self.info.hours:
                self.dialog.widgets.time_unit_label.text = _('Hour(s)')

        self.validate()
        self.dialog.resize(1, 1)

    def validate(self, text=''):
        self.dialog.widgets.error_label.hide()

        once_text = ''
        from_text = ''
        to_text = ''
        if APP.is_gtk():
            self.dialog.widgets.time_once_edit.hide_error()
            self.dialog.widgets.time_from_edit.hide_error()
            self.dialog.widgets.time_to_edit.hide_error()

            once_text = self.dialog.widgets.time_once_edit.text
            from_text = self.dialog.widgets.time_from_edit.text
            to_text = self.dialog.widgets.time_to_edit.text

        elif APP.is_pyside():
            once_text = self.dialog.widgets.time_once_timepicker.time
            from_text = ''
            to_text = ''

            if self.dialog.widgets.time_from_check.checked:
                from_text = self.dialog.widgets.time_from_timepicker.time
                to_text = self.dialog.widgets.time_to_timepicker.time

        error = self.info.validate_time(self.dialog.widgets.time_combo.selected,
            once_text, from_text, to_text)

        if error == self.info.once_error and APP.is_gtk():
            self.dialog.widgets.time_once_edit.show_error()
        elif error == self.info.from_error and APP.is_gtk():
            self.dialog.widgets.time_from_edit.show_error()
        elif error == self.info.to_error and APP.is_gtk():
            self.dialog.widgets.time_to_edit.show_error()
        elif error == self.info.from_to_error and APP.is_gtk():
            self.dialog.widgets.time_from_edit.show_error()
            self.dialog.widgets.time_to_edit.show_error()
        elif error == self.info.order_error:
            self.dialog.widgets.error_label.show()

        if error == self.info.no_error:
            self.dialog.widgets.ok_button.enable()
        else:
            self.dialog.widgets.ok_button.disable()

    def ok(self):
        once_text = ''
        from_text = ''
        to_text = ''
        if APP.is_gtk():
            once_text = self.dialog.widgets.time_once_edit.text
            from_text = self.dialog.widgets.time_from_edit.text
            to_text = self.dialog.widgets.time_to_edit.text
        elif APP.is_pyside():
            once_text = self.dialog.widgets.time_once_timepicker.time
            from_text = ''
            to_text = ''

            if self.dialog.widgets.time_from_check.checked:
                from_text = self.dialog.widgets.time_from_timepicker.time
                to_text = self.dialog.widgets.time_to_timepicker.time

        self.done.emit(self.info.build_time(self.dialog.widgets.time_combo.selected,
            once_text, self.dialog.widgets.time_every_spin.value, from_text, to_text))

        self.dialog.close()
